<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$|(RX_0Nfmm)>+:!>R+G |Ral_Vy!~]&62t4WhM5r(VI+?GV cKZcxbNn5&m/7yX');
define('SECURE_AUTH_KEY',  '*SiP5-DfBZrM)#Bs]R][0$[~{sF$}-wG`;kEPX/G6r={6wP$$Z5JmO`Gt]b!a9ie');
define('LOGGED_IN_KEY',    '*$PixnZ~o+I^1|/!fS:g,-sNy>aW_ M^-|?z[>d*`iJ%_E|Wx[he8-O1S/-m*CMA');
define('NONCE_KEY',        '!?|7>&|BAx~9xO|&~/>9hg^  ;wtB+nUj8|B@pQ(uzFq+oeiC68F;d$;r^e$t;|c');
define('AUTH_SALT',        'NV6#|QE+K*nQ>7E$WOw]T[eLahw@T9xI1DU*,>Fe{[!|Z,BMcO)+I2,a+U)sO-Ub');
define('SECURE_AUTH_SALT', 'ZBKcEdQ]cpm+X S=Czz0m@{gQF~Bfym=H@^X(!QW7PK8WEdW5c7viWpa9q^O+~r9');
define('LOGGED_IN_SALT',   'Dqmsd@grI,y5`6#kX=u9r@HuI=BZaM;_cvYs;fSkCzzJ;uI_FUW>X%rWr(S7X:^7');
define('NONCE_SALT',       'MYnW7(+Ez1|9t9R;B<yns8WjY(#{YqE-nm+doC;%yXp^2sAG-j#WrM_cnEUDyL:~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
